package com.example.demo.repository;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  {

    @Select("SELECT u.email,u.password,l.role FROM lh_user u \n" +
            "INNER JOIN \n" +
            "lh_user_role r on r.user_id=u.id \n" +
            "inner JOIN lh_role l  on l.id=r.role_id where u.email=#{email}")
    @Results({
            @Result(property = "roles.role",column = "l.role")
    })
    User loadUserByEmail(String email);

    /*@Select("SELECT email,password from lh_user where email like #{email}")
    @Results({
            @Result(property = "roles", column = "id" ,
                    many= @Many(select = "com.example.demo.repository.RoleRepository.findRoleByUserId")
            )
    })*/
}
