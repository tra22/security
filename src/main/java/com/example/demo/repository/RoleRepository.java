package com.example.demo.repository;

import com.example.demo.model.Role;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository {
    @Select("Select  r.id, r.role from lh_user_role ur INNER JOIN lh_role r ON ur.role_id=r.id where ur.user_id=#{id}")
    @Results(value = {
            @Result(property = "role", column = "role")
    })
    public List<Role> findRoleByUserId(int userId);


}
