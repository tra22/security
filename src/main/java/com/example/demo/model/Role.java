package com.example.demo.model;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {
    private int id;
    private String role;
    private User user;
    private static final long serialVersionUID = 1L;

    public Role(int id, String role) {
        this.id = id;
        this.role = role;
    }

    public Role(int id, String roles, User user) {
        this.id = id;
        this.role = roles;
        this.user = user;
    }

    public Role() {

    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getAuthority() {
        return  "ROLE_"+role;
    }
}
